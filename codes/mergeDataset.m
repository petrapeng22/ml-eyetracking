function mergeDataset()
    %labelFile = matfile("../data/allLabels_S_all.mat");
    %featureFile = matfile("../data/allFeatures_S_all.mat");
    %sets = [11,12,13,14,21,22,23,31,32,33];
    sets = ["_S_all.mat","2.mat"];
    allFeatures=[];
    allLabels=[];
   
    
    for i = 1:2
        %labelFileName = "Labels" + sets(1,i) + ".mat"
        labelFile = matfile("../data/allLabels" + sets(1,i));
        featureFile = matfile("../data/allFeatures" + sets(1,i));
        allLabels = [allLabels; labelFile.allLabels];
        allFeatures = [allFeatures; featureFile.allFeatures];
    end
    save('../data/allLabels_2.mat','allLabels');
    save('../data/allFeatures_2.mat','allFeatures');
end
