function processRes()
    set1=["ini","st"];
    set2=["0", "1","2","3"];
    %set3=["42","123","456"];
    means = [];
    stds = [];
    accs2 = [];
    accs3 = [];
    accs5 = [];
    for i=1:2
        for j = 1:4
            
                fileName = "..\exp-results\result_" + set1(1,i) + set2(1,j) + ".txt";
                
                file = fopen(fileName);
                
                formatSpec = '%f';
                result=fscanf(file,formatSpec);
                acc2 = 1 - sum(sum(result > 2))/600;
                acc3 = 1 - sum(sum(result > 3))/600;
                acc5 = 1 - sum(sum(result > 5))/600;
                accs2 = [accs2, acc2];
                accs3 = [accs3, acc3];
                accs5 = [accs5, acc5];
                means = [means, mean(result)];
                stds = [stds,std(result)];
                
%                 resultMean = (result1 + result2 + result3)/3;
%                 result_name = "..\exp-results\result_" + set1(1,i) + set2(1,j) + ".txt";
%                 fid = fopen(result_name,'wt');
%                 fprintf(fid,'%g\n',resultMean);      
%                 fclose(fid);
        end
    end
    means
    stds
    accs2
    accs3
    accs5
    
end