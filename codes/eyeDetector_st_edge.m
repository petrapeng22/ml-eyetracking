function [] = eyeDetector_st(seed)
    labelFile = matfile("../data/allLabels_2.mat");
    featureFile = matfile("../data/allFeatures_2.mat");
    allLabels = labelFile.allLabels; 
    allFeatures = featureFile.allFeatures;
    
    edgeFeatures= matfile("../data/allEdges.mat");
    allEdges = edgeFeatures.allEdges;
    allFeatures = cat(4, reshape(allFeatures, 2000, 36, 64, 1), reshape(allEdges, 2000, 36, 64, 1));
    
    % shuffle the dataset with labels
    % hualide fengexian-----
    rng(42);
    start_idx = randperm(100);
    train_idx = start_idx(1:70);
    eval_idx = start_idx(71:100);
    for i=2:20
        temp_idx = randperm(100);
        temp_idx = temp_idx+100 * (i-1);
        train_temp_idx = temp_idx(1:70);
        eval_temp_idx = temp_idx(71:100);
        
        train_idx = [train_idx, train_temp_idx];
        eval_idx = [eval_idx, eval_temp_idx];    
    end
    
    

    % hualide fengexian-----
    
    
    % get training data and labels
    trainingFeatures=allFeatures(train_idx,:,:,:);
    trainingLabels=allLabels(train_idx,:);    
    
    % get evaluation data and labels
    evalFeatures=allFeatures(eval_idx,:,:,:);
    evalLabels=allLabels(eval_idx,:);  
    
    %labelFile = matfile("../data/evalLabels.mat");
    %featureFile = matfile("../data/evalFeatures.mat");
    %evalFeatures=featureFile.evalFeatures;
    %evalLabels=labelFile.evalLabels;
    
    %save('../data/evalFeatures.mat','evalFeatures');
    %save('../data/evalLabels.mat','evalLabels');
    
    rng(seed);
    disp(randperm(5));
    % data processing
    XX_T_N=[];
    for it1=1:size(trainingFeatures,1) % size of the 1. demension of trainingFeatures = number of training data
        for it2=1:size(trainingFeatures,2) % 2. demension of trainingFeatures = height of the image (1:36)
            for it3=1:size(trainingFeatures,3) % 3. demension of trainingFeatures = width of the image (1:48)
                for it4=1:2
                    XX_T_N(it2,it3,it4,it1)=trainingFeatures(it1,it2,it3,it4); % reshape the matrix
                end
            end
        end
    end
    
    XX_V_N=[];
    for it1=1:size(evalFeatures,1)
        for it2=1:size(evalFeatures,2)
            for it3=1:size(evalFeatures,3)
                for it4=1:2
                    XX_V_N(it2,it3,it4,it1)=evalFeatures(it1,it2,it3,it4);
                end
            end
        end
    end
   
 % train network
%     layers = [ ...
%     imageInputLayer([size(trainingFeatures,2) size(trainingFeatures,3) 2]) ...
%     convolution2dLayer(3,32) ...
%     reluLayer ...
%     maxPooling2dLayer(2) ...
%     convolution2dLayer(3,64) ...
%     reluLayer ...
%     maxPooling2dLayer(2) ...
%     convolution2dLayer(3,128) ...
%     reluLayer ...
%     maxPooling2dLayer(2) ...
%     fullyConnectedLayer(512) ...
%     fullyConnectedLayer(2) ...
%     regressionLayer];
%     
% 
% 
% 
%     miniBatchSize  = 16;
%     validationFrequency = floor(size(trainingFeatures,1)/miniBatchSize);
% 
% 
%     options = trainingOptions('adam',...
%         'MiniBatchSize',miniBatchSize,...
%         'MaxEpochs',100,...
%         'InitialLearnRate',1e-4,...
%         'LearnRateSchedule','piecewise',...
%         'LearnRateDropFactor',0.1,...
%         'LearnRateDropPeriod',10000,...
%         'Shuffle','every-epoch',...
%         'ValidationData',{XX_V_N,evalLabels},...
%         'ValidationFrequency',validationFrequency,...
%         'ValidationPatience',Inf,...
%         'Plots','training-progress',...
%         'Verbose',false);
%     
% 
%     net = trainNetwork(XX_T_N,trainingLabels,layers,options);

% -----------------------------------------------------------------
    layers = [
    imageInputLayer([size(trainingFeatures,2) size(trainingFeatures,3) 2],'Name','input')
    
    convolution2dLayer(5,16,'Padding','same','Name','conv_1')
    batchNormalizationLayer('Name','BN_1')
    leakyReluLayer(0.2, 'Name','relu_1')
    
    convolution2dLayer(3,32,'Padding','same','Stride',2,'Name','conv_2')
    batchNormalizationLayer('Name','BN_2')
    leakyReluLayer(0.2, 'Name','relu_2')
    convolution2dLayer(3,32,'Padding','same','Name','conv_3')
    batchNormalizationLayer('Name','BN_3')
    leakyReluLayer(0.2, 'Name','relu_3')
    
    additionLayer(2,'Name','add')
    
    averagePooling2dLayer(2,'Stride',2,'Name','avpool')
    fullyConnectedLayer(512,'Name','fc_0')
    leakyReluLayer(0.2, 'Name','relu_4')
    dropoutLayer(0.5, 'Name', 'dr')
    fullyConnectedLayer(2,'Name','fc')
    regressionLayer('Name', 'reg')];
    
    skipConv = convolution2dLayer(1,32,'Stride',2,'Name','skipConv');
    lgraph = layerGraph(layers);
    lgraph = addLayers(lgraph,skipConv);
    lgraph = connectLayers(lgraph,'relu_1','skipConv');
    lgraph = connectLayers(lgraph,'skipConv','add/in2');
    
    figure
    plot(lgraph);

    miniBatchSize  = 128;
    validationFrequency = floor(size(trainingFeatures,1)/miniBatchSize);
    validationFrequency = 20;


    options = trainingOptions('adam',...
        'MiniBatchSize',miniBatchSize,...
        'MaxEpochs',300,...
        'ExecutionEnvironment','auto',...
        'InitialLearnRate',1e-3,...
        'LearnRateSchedule','piecewise',...
        'LearnRateDropFactor',1e-1,...
        'LearnRateDropPeriod',10000,...
        'Shuffle','every-epoch',...
        'ValidationData',{XX_V_N,evalLabels},...
        'ValidationFrequency',validationFrequency,...
        'ValidationPatience',Inf,...
        'Plots','training-progress',...
        'Verbose',false);
    

    net = trainNetwork(XX_T_N,trainingLabels,lgraph,options);
% -----------------------------------------------------------------
    
    YPredicted = predict(net,XX_V_N); % predict the position of pupil center of each image in test data
    prediction_name = strcat('predicted_ini3_seed_', int2str(seed), '.mat');
    %save(prediction_name,'YPredicted');
    
    % evaluate the result(mean, std...)
    val1=(YPredicted(:,1)-evalLabels(:, 1)).^2; % compute the error of x positions
    val2=(YPredicted(:,2)-evalLabels(:, 2)).^2; % compute the error of y positions
    vals=sqrt(val1+val2); 
    mean(vals)
    std(vals)
    % save the results to txt
    result_name = strcat('result_ini3_seed_', int2str(seed), '.txt');
    %fid = fopen(result_name,'wt');
    %fprintf(fid,'%g\n',vals);      
    %fclose(fid);
end
