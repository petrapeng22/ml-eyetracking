function generateEvalVedio()
    poolobj = gcp('nocreate');
    delete(poolobj);
    
    % save the result video
    video_wr = VideoWriter('../exp-results/evaluation_ini0.avi');
    open(video_wr);
    
    predFile = matfile("../exp-results/predicted_ini0_seed_42.mat");
    evalFile = matfile("../data/evalFeatures.mat");
    YPredicted = predFile.YPredicted; 
    evalFeatures = evalFile.evalFeatures;
    
    for evalId = 1:size(evalFeatures,1) % go through all the evalutate images
        img(:,:) =  evalFeatures(evalId,:,:);
        kx = YPredicted(evalId,1); % get the predicted x position of the pupil
        ky = YPredicted(evalId,2); % get the predicted y position of the pupil
        figure(1);
        imshow(img)
        hold on;
        plot(kx,ky,'r.', 'LineWidth', 2, 'MarkerSize', 3); % draw a point at the detected position
        F = getframe(gcf); % get the plot with the point we draw
        writeVideo(video_wr,F); % write it to video
        hold off;    
    end
    close(video_wr);
end
