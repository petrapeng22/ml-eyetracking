function annotation()
   
    video = VideoReader('camvideo21.avi');
    numOfFrames = video.NumberOfFrames;
    
    % for writing images to a video file ----------------
    %video_wr = VideoWriter('annotationVideo.avi');  
    %open(video_wr); 
    % ---------------------------------------------------
    
    % Open text File for saving positions
    fileID = fopen('Annotation21.txt','w');
    
    for i=1:numOfFrames
        img= read(video,i);
        
        % showing img and plotting red dot and saving clicked position
        figure(1);
        imshow(img);
        hold on;
        [x,y] = ginput(1);
        plot1 = plot(x,y,'r.', 'LineWidth', 2, 'MarkerSize', 5);
        
        % name of image and position as strings
        imgName = "ImageNo" + i;
        xPos = x;
        yPos = y;
        
        % Reannotation due to while loop
        state = true;
 
        while state
            [xCorrect,yCorrect] = ginput(1);
             
            if (xCorrect<0)
                delete(plot1);
                [newx, newy] = ginput(1);
                plot1 = plot(newx,newy,'r.', 'LineWidth', 2, 'MarkerSize', 5);
                xPos = newx;
                yPos = newy; 
            elseif (yCorrect<0)
                state = false;
             end
        end
        
        textLine = imgName + ";" + xPos + ";" +yPos;
       
        % Test: printing out each line on console
        disp(textLine);    
      
        % Writing positions to text file
        % %s for string input, \n for ending each line
        fprintf(fileID,'%s\n', textLine);   
        
        % for writing images to a VIDEO FILE
        %writeVideo(video_wr,img);  
        
        % writing images to a FOLDER as png files
        % FILENAME = 'D:/UNI T�bingen\MedizinInfo\19-SS_Medizininformatik\TEAMPROJEKT\Task01\task1\annotationImages\' + imgName + '.png';
        % imwrite(img,FILENAME);
        hold off;
    end
    
    % Close text file
    fclose(fileID); 
    
    % Closing video writer
    %close(video_wr);
    
end
