function splitDataset()
    labelFile = matfile("../data/allLabels_2.mat");
    featureFile = matfile("../data/allFeatures_2.mat");
    allLabels = labelFile.allLabels; 
    allFeatures = featureFile.allFeatures;
    evalFeatures=[];
    evalLabels=[];
    for i= 0:19
        for j = 1:30
            idx = 100*i + 70 + j;
            evalFeatures = [evalFeatures; allFeatures(idx,:,:)];
            evalLabels = [evalLabels; allLabels(idx,:)];
        end
    end
    save('../data/evalFeatures.mat','evalFeatures');
    save('../data/evalLabels.mat','evalLabels');
end