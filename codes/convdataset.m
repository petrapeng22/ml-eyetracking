function convdataset()

    %sets{1,1}='train';
    %sets{1,2}='eval';

    %for i=1:2
        %video_wr = VideoWriter([sets{1,i} '_out.avi']);
        %open(video_wr);
        video = VideoReader('camvideo32.avi');
    
        fi=fopen('Annotation32.txt');
        %fgetl(fi);
        i = 1;
        while ~feof(fi)
            line = fgetl(fi);
            arr=strsplit(line,';');
            akx=str2double(arr{1,2});
            aky=str2double(arr{1,3});
        
            img = readFrame(video);
            [y x c]=size(img);
        
            for k1=-1:1
                for k2=-1:1
                    xx=round(akx+k2);
                    yy=round(aky+k1);
                    if xx<x+1 && yy<y+1 && xx>0 && yy>0
                        img(yy,xx,1)=255;
                        img(yy,xx,2)=0;
                        img(yy,xx,3)=0;
                    end
                end
            end
            imgName = "image" + i;
            %writeVideo(video_wr,img);
            % FILENAME = 'D:/UNI T�bingen\MedizinInfo\19-SS_Medizininformatik\TEAMPROJEKT\Task01\task1\annotationImages\' + imgName + '.png';
            fileName = "/Users/petra/Desktop/32/" + imgName + ".png";
            imwrite(img,fileName);
            i = i + 1;
        end
    
        fclose(fi);
        %close(video_wr);
    %end

end

