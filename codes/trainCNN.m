function trainCNN()
    sets{1,1}='train';
    sets{1,2}='eval';
    
    trainingFeatures=[];
    trainingLabels=[];
    training_idx=0;

    for i=1:1
        video = VideoReader([sets{1,i} '.avi']);
    
        fi=fopen([sets{1,i} '.txt']);
        fgetl(fi);
        
        stop_i=0;
        % sample our training data from the first 3000 frames of the video
        while ~feof(fi) && stop_i<3000 % at most read 3000 frames
            stop_i=stop_i+1;
            
            
            line = fgetl(fi);
            img = readFrame(video);
            
            
            
            skip_i=0;
            while ~feof(fi) && skip_i<30 % read a frame every 30 frames
                line = fgetl(fi);
                img = readFrame(video);
                skip_i=skip_i+1;
            end
            
            
            
            arr=strsplit(line,';');
            akx=str2double(arr{1,1});
            aky=str2double(arr{1,2});
        
            img = rgb2gray(img);
            [iy ix]=size(img);
            img=imresize(img,[iy/4 ix/4]);
            
            img=(double(img)/256.0);
                        
            training_idx=training_idx+1;
            % store the whole image (every pixel) as feature with the corresponding id
            trainingFeatures(training_idx, :, :) = img(:,:);
            % store the x-, y-coridinate as the labels of an image
            trainingLabels(training_idx, 1) = akx/4; % x-coordinate of the annotated pupil center
            trainingLabels(training_idx, 2) = aky/4; % y-coordinate of the annotated pupil center
                        
            
        end
    
        fclose(fi);
    end
    
    
    evalFeatures=[];
    evalLabels=[];
    eval_idx=0;

    for i=1:1
        video = VideoReader([sets{1,i} '.avi']);
    
        fi=fopen([sets{1,i} '.txt']);
        fgetl(fi);
        
        
        skip_i=0;
        while ~feof(fi) && skip_i<3500 % skip the first 3500 frames
            line = fgetl(fi);
            img = readFrame(video);
            skip_i=skip_i+1;
        end
        
        
        % sample our test data from the 3500th frame to the end of the
        % video
        while ~feof(fi) 
%             stop_i=stop_i+1;
            
            
            line = fgetl(fi);
            img = readFrame(video);
            
            skip_i=0;
            while ~feof(fi) && skip_i<30 % read a frame every 30 frames
                line = fgetl(fi);
                img = readFrame(video);
                skip_i=skip_i+1;
            end
            
            
            arr=strsplit(line,';');
            akx=str2double(arr{1,1})/4;
            aky=str2double(arr{1,2})/4;
        
            
            img = rgb2gray(img);
            [iy ix]=size(img);
            img=imresize(img,[iy/4 ix/4]);
            
            
            img=(double(img)/256.0); 
                        
            eval_idx=eval_idx+1;
            evalFeatures(eval_idx, :,:) = img(:,:); 
            evalLabels(eval_idx, 1) = akx;
            evalLabels(eval_idx, 2) = aky;
        end
    
        fclose(fi);
    end
    
    XX_T_N=[];
    for it1=1:size(trainingFeatures,1) % size of the 1. demension of trainingFeatures = number of training data
        for it2=1:size(trainingFeatures,2) % 2. demension of trainingFeatures = height of the image (1:36)
            for it3=1:size(trainingFeatures,3) % 3. demension of trainingFeatures = width of the image (1:48)
                XX_T_N(it2,it3,1,it1)=trainingFeatures(it1,it2,it3); % reshape the matrix
            end
        end
    end
    
    XX_V_N=[];
    for it1=1:size(evalFeatures,1)
        for it2=1:size(evalFeatures,2)
            for it3=1:size(evalFeatures,3)
                XX_V_N(it2,it3,1,it1)=evalFeatures(it1,it2,it3);
            end
        end
    end
    
    
	
	
    %train network
    layers = [ ...
    imageInputLayer([size(trainingFeatures,2) size(trainingFeatures,3) 1]) ...
    convolution2dLayer(3,32) ...
    reluLayer ...
    maxPooling2dLayer(2) ...
    convolution2dLayer(3,64) ...
    reluLayer ...
    maxPooling2dLayer(2) ...
    convolution2dLayer(3,128) ...
    reluLayer ...
    maxPooling2dLayer(2) ...
    fullyConnectedLayer(512) ...
    fullyConnectedLayer(2) ...
    regressionLayer];
    



    miniBatchSize  = 10;
    validationFrequency = floor(size(trainingFeatures,1)/miniBatchSize);


    options = trainingOptions('adam',...
        'MiniBatchSize',miniBatchSize,...
        'MaxEpochs',100,...
        'InitialLearnRate',1e-4,...
        'LearnRateSchedule','piecewise',...
        'LearnRateDropFactor',0.1,...
        'LearnRateDropPeriod',10000,...
        'Shuffle','every-epoch',...
        'ValidationData',{XX_V_N,evalLabels},...
        'ValidationFrequency',validationFrequency,...
        'ValidationPatience',Inf,...
        'Plots','training-progress',...
        'Verbose',false);
    

    net = trainNetwork(XX_T_N,trainingLabels,layers,options);
    
    YPredicted = predict(net,XX_V_N); % predict the position of pupil center of each image in test data
    
    
    
    val1=(YPredicted(:,1)-evalLabels(:, 1)).^2; % compute the error of x positions
    val2=(YPredicted(:,2)-evalLabels(:, 2)).^2; % compute the error of y positions
    vals=sqrt(val1+val2); 
    mean(vals)
    std(vals)
    fid = fopen('svm-result.txt','wt');
    fprintf(fid,'%g\n',vals);      
    fclose(fid);
    
    [YPredicted(:,1) YPredicted(:,2) evalLabels(:, 1) evalLabels(:, 2)]
    
    % Task 2
    % 1. We save the image itself as training or evaluation data instead
    %    of HOG features of the image.
    % 2. In last script, what we predict is whether an subimage contains the pupil,
    %    but this time we predict directly the x, y positions.
    % 3. We use a different learning algorithm, last time we do classification but
    %    this time we do regression.
     
    
    % Task 3
    % draw the detected center position into the image and write it to a
    % video file.
    video_wr = VideoWriter('evaluation.avi');
    open(video_wr);
    for evalId = 1:size(evalFeatures,1) % go through all the evalutate images
        img(:,:) =  evalFeatures(evalId,:,:);
        kx = YPredicted(evalId,1); % get the predicted x position of the pupil
        ky = YPredicted(evalId,2); % get the predicted y position of the pupil
        figure(1);
        imshow(img)
        hold on;
        plot(kx,ky,'r.', 'LineWidth', 2, 'MarkerSize', 3); % draw a point at the detected position
        F = getframe(gcf); % get the plot with the point we draw
        writeVideo(video_wr,F); % write it to video
        hold off;
    end
    close(video_wr);

end



