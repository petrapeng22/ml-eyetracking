## Chanllenges

#### placement of the camera(angles)

- camera above the eye (eyelash will cover the eye) 
- camera under the eye 
- camera on the left/right side of the eye 
- camera facing the eye 

#### changing light conditions

- Normal room light 
- Dark light #
- Bright light (with reflections in the eye) #

#### other challenges

- Camera shaking (whole eye moves around in video) #

- Eye with glasses
- Different eye shape (squint, blink)



In total 10 challenges. 100 frames per challenge per person -> 100 * 10 * 2 = 2000 images.

1000 images for Training data, 1000 images for Evaluation data