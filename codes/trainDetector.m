function trainDetector()

    sets{1,1}='train';
    sets{1,2}='eval';
    
    sub_img_win=8;          % setting size for sub-image to 8 pixels
    cellSize = [4 4];       % initializing cellSize-parameter for extractHOGFeatures-function later
    trainingFeatures=[];    % declaring array in which the features of one sub-image are stored
    trainingLabels=[];      % declaring array in which the class label of each training_id is stored
    training_idx=0;         % initializing counter for training-ids

    for i=1:1      
        video = VideoReader([sets{1,i} '.avi']);    % opening the video file
    
        fi=fopen([sets{1,i} '.txt']);               % opening the according text file with the pupile positions
        fgetl(fi);                                  % getting a line of the text file
        
        while ~feof(fi)         % going through all the lines as long as there are lines


            line = fgetl(fi);               % storing one line of the text file 
            img = readFrame(video);         % storing one image (frame) of the video
            
            stop_i=0;                       % initializing frame-counter
            
            while ~feof(fi) && stop_i<100   % skip 100 lines and 100 frames
                stop_i=stop_i+1;            % counting up the frame-counter...  
                line = fgetl(fi);           % skipping the lines...
                img = readFrame(video);     % skipping the frames...
            end
            
            arr=strsplit(line,';');         % splitting line to get just the x- and y-positions as strings
            akx=str2double(arr{1,1})/4;     % convert x-position to a double and dividing it by 4 
            aky=str2double(arr{1,2})/4;     % convert y-position to a double and dividing it by 4
        
            img = rgb2gray(img);            % convert colour image to gray image   144 * 193 Unit 8 (integer)
            [iy ix]=size(img);              % getting width (iy) and height (ix)
            img=imresize(img,[iy/4 ix/4]);  % resizing image to 1/16 of its original size   36 * 48 Unit 8 (integer)
            iy=iy/4;                        % adjusting image-width
            ix=ix/4;                        % adjusting image-height
            
            
            % valid examples: In this loop we take max. 9 sub-images from near
            % the center of the annotated pupil-position if the sub-images
            % are inside of the frames window
            % Then we do some image processing and save the
            % features of the sub-image with its training-id
            % We assign the label 1, it shows that the sub-img contains the pupil
            
            for k1=-1:1
                for k2=-1:1
                    raky=round(aky+k1);     
                    rakx=round(akx+k2);
                    
                    % checking if raky- and rakx-position are inside the frame-window
                    if raky-sub_img_win>0 && rakx-sub_img_win>0 && ...      
                            raky+sub_img_win<iy && rakx+sub_img_win<ix
                        
                        % taking a sub-image coming from a position (raky, rakx) with a width and height of 16 pixels
                        % (sub_img_win = 8) 2sub_img_win * 2sub_img_win
                        sub_img=img(raky-sub_img_win:raky+sub_img_win, rakx-sub_img_win:rakx+sub_img_win);      
                        
                        % image processing
                        sub_img=double(sub_img)./255.0;
                        sub_img=sub_img-mean(sub_img(:));
                        
                        % counting up one training-id
                        training_idx=training_idx+1;   
                        % saving features of that sub-image as training data
                        % into histograms (linear data)
                        trainingFeatures(training_idx, :) = extractHOGFeatures(sub_img, 'CellSize', cellSize);   %feature 
                        % saving the trainingLabel "1" as a sign that the sub-image was taken from near the center of the pupil
                        trainingLabels(training_idx, 1) = 1;    %class label
                    end
                    
                end
            end
            
            % invalid examples: in this loop we take max. 64 sub-images
            % further away from the center of the annotated pupil-position
            % if the sub-images are inside of the frame-window
            % Then again doing some image processing on the sub-image and saving the
            % features of the sub-image as training data with its training-id
            % We assign the label 0, it shows that the sub-img doesnt
            % contain the pupil
            for k1=-10:2:10
                for k2=-10:2:10
                    
                    % only taking positions -10 -8 -6 -4  4  6  8  10 pixels away from the annotated pupil-center
                    if abs(k1)>2 && abs(k1)>2
                        raky=round(aky+k1);
                        rakx=round(akx+k2);
                        
                        % checking if raky- and rakx-position are inside the frame-window
                        if raky-sub_img_win>0 && rakx-sub_img_win>0 && ...
                                raky+sub_img_win<iy && rakx+sub_img_win<ix
                            
                            % taking a sub-image coming from a position (raky, rakx) with a width and height of 16 pixels
                            % (sub_img_win = 8) 2sub_img_win * 2sub_img_win
                            sub_img=img(raky-sub_img_win:raky+sub_img_win, rakx-sub_img_win:rakx+sub_img_win);
                    
                            % image processing
                            sub_img=double(sub_img)./255.0;
                            sub_img=sub_img-mean(sub_img(:));
                    
                            training_idx=training_idx+1;
                            trainingFeatures(training_idx, :) = extractHOGFeatures(sub_img, 'CellSize', cellSize);   %feature
                            
                            trainingLabels(training_idx, 1) = 0;    % class label
                        end
                    end
                end
            end
            
            
                        
        end
    
        fclose(fi);
    end
    
    % printing out the size of the arrays
    size(trainingFeatures)      % 3579 * 324 --> 3579 different training-ids * 324 different image-features
    size(trainingLabels)        % 3579 * 1   --> 3579 training-ids (datas) * 1 label (either 1 or 0)
    
    
    
    evalFeatures=[];
    eval_idx=0;

    for i=1:1   %%<----------- same subject
        video = VideoReader([sets{1,i} '.avi']);
    
        fi=fopen([sets{1,i} '.txt']);
        fgetl(fi);
        

        while ~feof(fi)
            
            line = fgetl(fi);
            img = readFrame(video);
            
            stop_i=0;
            % get a frame every 110 frames, so we get different images than what we get as training data
            while ~feof(fi) && stop_i<110   % skip
                stop_i=stop_i+1;
                line = fgetl(fi);
                img = readFrame(video);
            end
            
            
            
            arr=strsplit(line,';');
            akx=str2double(arr{1,1})/4;
            aky=str2double(arr{1,2})/4;
        
            img = rgb2gray(img);
            [iy ix]=size(img);
            img=imresize(img,[iy/4 ix/4]);
            
            % storing attributes img, akx, aky in evalFeatures at given eval-id-index            
            eval_idx=eval_idx+1;
            evalFeatures(eval_idx,1).img = img;  
            evalFeatures(eval_idx,1).akx = akx;  
            evalFeatures(eval_idx,1).aky = aky; 
        end
    
        fclose(fi);
    end

    % printing out the size of evalFeatures-data-sturcture
    size(evalFeatures)      % 55 * 1 --> 55 frames with their stored attributes
    
    
    
    % running one of the following 3 machine learning algorithms
    t = templateSVM('Standardize',true);
    % --> performs fastest and accurate
    
    % t = templateTree('surrogate','on');
    % --> performs very slow and relatively unaccurate

    % t = templateKNN('NumNeighbors',3,'Standardize',1); warning ('off','all');
    % --> performs slow und relatively accurate
    
    % returns a trained ECOC model using the predictors 'trainingFeatures'
    % and the class labels 'trainingLabels'
    Mdl = fitcecoc(trainingFeatures,trainingLabels,'Learners',t);
    
    % printing out that training is done
    'train done'
    
    preX = [];
    preY = [];
    trueX = [];
    trueY = [];
    
    for i=1:size(evalFeatures,1)
        akimg=evalFeatures(i,1).img;    % taking the frame-image we saved
        [sy sx]=size(akimg);
        
        % saving frame in a variable onto which a dot can be added
        resultmap=akimg;
        trueX = [trueX,evalFeatures(i,1).akx];  
        trueY = [trueY,evalFeatures(i,1).aky];  
            
        % for computing the average position of the white dots ------------
        ky = 0;     
        kx = 0;
        
        cnt = 0;    % counting amount of predicted points as class 1
        % -----------------------------------------------------------------
        
        % go through (scan) all the different sub-images with size 16 * 16
        % pixels within the frame-window
        % (frame-size 36*48)
        for k1=sub_img_win+1:sy-(sub_img_win+1)         
            for k2=sub_img_win+1:sx-(sub_img_win+1)     
                
                sub_img=akimg(k1-sub_img_win:k1+sub_img_win, k2-sub_img_win:k2+sub_img_win);
                
                sub_img=double(sub_img)./255.0;
                sub_img=sub_img-mean(sub_img(:));
                
                % extracting all the stored features of one sub-image
                X(1,:)=extractHOGFeatures(sub_img, 'CellSize', cellSize);
                
                % checking if predict-function would predict pupil to be in
                % a certain position (k1,k2)
                if(predict(Mdl,X)==1)
                    % if so draw a white dot at that position  --> makes areas visible where computer predicts pupil to be
                    % resultmap(k1,k2) = 255;
                    
                    % average pos --> calculating center of the predicted areas
                    ky = ky + k1;
                    kx = kx + k2;
                    
                    cnt = cnt + 1;
                    % ----------------------------------------------------
                    
                    
                end
            end
        end
        
        % calculating average predicted position
        if cnt == 0 % ???? what should the distance be when there is no predicted position????
            ky = 0;
            kx = 0;
        else
            
            ky = ky / cnt;
            kx = kx / cnt;
        end
        preX = [preX,kx];
        preY = [preY,ky];
        
% %         figure(1);
% %         imshow(akimg);
        
        % drawing just the average predicted pupil position onto the frame
        %figure(2);
        %imshow(resultmap);
        %hold on;
        %plot(kx, ky,'r.', 'LineWidth', 2, 'MarkerSize', 5);
        %hold off;
        
    
    end
    preY
    trueY
    val1=(preY-trueY).^2
    val2=(preX-trueX).^2
    vals=sqrt(val1+val2)
    mean(vals)
    std(vals)


end

