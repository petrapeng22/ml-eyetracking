function iniDetector(seed)
    labelFile = matfile("../data/allLabels_1.mat");
    featureFile = matfile("../data/allFeatures_1.mat");
    allLabels = labelFile.allLabels; 
    allFeatures = featureFile.allFeatures;
    
    % shuffle the dataset with labels
    % hualide fengexian-----
    rng(42);
    start_idx = randperm(100);
    train_idx = start_idx(1:70);
    eval_idx = start_idx(71:100);
    for i=2:30
        temp_idx = randperm(100);
        temp_idx = temp_idx+100 * (i-1);
        train_temp_idx = temp_idx(1:70);
        eval_temp_idx = temp_idx(71:100);
        
        train_idx = [train_idx, train_temp_idx];
        eval_idx = [eval_idx, eval_temp_idx];    
    end
    
    

    % hualide fengexian-----
    
    
    % get training data and labels
    trainingFeatures=allFeatures(train_idx,:,:);
    trainingLabels=allLabels(train_idx,:);    
    
    % get evaluation data and labels
    %evalFeatures=allFeatures(eval_idx,:,:);
    %evalLabels=allLabels(eval_idx,:);  
    
    labelFile = matfile("../data/evalLabels.mat");
    featureFile = matfile("../data/evalFeatures.mat");
    evalFeatures=featureFile.evalFeatures;
    evalLabels=labelFile.evalLabels;
    
    
    rng(seed);
    disp(randperm(5));
    % data processing
    XX_T_N=[];
    for it1=1:size(trainingFeatures,1) % size of the 1. demension of trainingFeatures = number of training data
        for it2=1:size(trainingFeatures,2) % 2. demension of trainingFeatures = height of the image (1:36)
            for it3=1:size(trainingFeatures,3) % 3. demension of trainingFeatures = width of the image (1:48)
                XX_T_N(it2,it3,1,it1)=trainingFeatures(it1,it2,it3); % reshape the matrix
            end
        end
    end
    
    XX_V_N=[];
    for it1=1:size(evalFeatures,1)
        for it2=1:size(evalFeatures,2)
            for it3=1:size(evalFeatures,3)
                XX_V_N(it2,it3,1,it1)=evalFeatures(it1,it2,it3);
            end
        end
    end
   
    % train network
    layers = [ ...
    imageInputLayer([size(trainingFeatures,2) size(trainingFeatures,3) 1]) ...
    convolution2dLayer(3,32) ...
    reluLayer ...
    maxPooling2dLayer(2) ...
    convolution2dLayer(3,64) ...
    reluLayer ...
    maxPooling2dLayer(2) ...
    convolution2dLayer(3,128) ...
    reluLayer ...
    maxPooling2dLayer(2) ...
    fullyConnectedLayer(512) ...
    fullyConnectedLayer(2) ...
    regressionLayer];
    



    miniBatchSize  = 16;
    validationFrequency = floor(size(trainingFeatures,1)/miniBatchSize);


    options = trainingOptions('adam',...
        'MiniBatchSize',miniBatchSize,...
        'MaxEpochs',100,...
        'InitialLearnRate',1e-4,...
        'LearnRateSchedule','piecewise',...
        'LearnRateDropFactor',0.1,...
        'LearnRateDropPeriod',10000,...
        'Shuffle','every-epoch',...
        'ValidationData',{XX_V_N,evalLabels},...
        'ValidationFrequency',validationFrequency,...
        'ValidationPatience',Inf,...
        'Plots','training-progress',...
        'Verbose',false);
    

    net = trainNetwork(XX_T_N,trainingLabels,layers,options);
    
    YPredicted = predict(net,XX_V_N); % predict the position of pupil center of each image in test data
    prediction_name = strcat('predicted_ini1_seed_', int2str(seed), '.mat');
    save(prediction_name,'YPredicted');
    
    % evaluate the result(mean, std...)
    val1=(YPredicted(:,1)-evalLabels(:, 1)).^2; % compute the error of x positions
    val2=(YPredicted(:,2)-evalLabels(:, 2)).^2; % compute the error of y positions
    vals=sqrt(val1+val2); 
    mean(vals)
    std(vals)
    % save the results to txt
    result_name = strcat('result_ini1_seed_', int2str(seed), '.txt');
    fid = fopen(result_name,'wt');
    fprintf(fid,'%g\n',vals);      
    fclose(fid);
    
    % save the result video
    %video_wr = VideoWriter('evaluation.avi');
    %open(video_wr);
    %for evalId = 1:size(evalFeatures,1) % go through all the evalutate images
        %img(:,:) =  evalFeatures(evalId,:,:);
        %kx = YPredicted(evalId,1); % get the predicted x position of the pupil
        %ky = YPredicted(evalId,2); % get the predicted y position of the pupil
        %figure(1);
        %imshow(img)
        %hold on;
        %plot(kx,ky,'r.', 'LineWidth', 2, 'MarkerSize', 3); % draw a point at the detected position
        %F = getframe(gcf); % get the plot with the point we draw
        %writeVideo(video_wr,F); % write it to video
        %hold off;
    %end
    %close(video_wr);
end
