function processRes2()
    set1=["ini","st"];
    set2=["0", "1","2","3"];
    set3=["42","123","456"];
    means = [];
    stds = [];
    accs2 = [];
    accs3 = [];
    accs5 = [];
    for i=1:2
        for j = 1:4
            for k = 1:3
                fileName = "..\exp-results\result_" + set1(1,i) + set2(1,j) + "_seed_" + set3(1,k) + ".txt";
                
                file = fopen(fileName);
                
                formatSpec = '%f';
                result=fscanf(file,formatSpec);
                acc2 = 1 - sum(sum(result > 2))/600;
                acc3 = 1 - sum(sum(result > 3))/600;
                acc5 = 1 - sum(sum(result > 5))/600;
                accs2 = [accs2, acc2];
                accs3 = [accs3, acc3];
                accs5 = [accs5, acc5];
                means = [means, mean(result)];
                
                
%                 resultMean = (result1 + result2 + result3)/3;
%                 result_name = "..\exp-results\result_" + set1(1,i) + set2(1,j) + ".txt";
%                 fid = fopen(result_name,'wt');
%                 fprintf(fid,'%g\n',resultMean);      
%                 fclose(fid);
            end
        end
    end
    %means2 = reshape(means,3,[])
    %means2
    %mm = mean(means2, 1)
    %stm = std(means2,1)
    
    accs2 = reshape(accs2,3,[]);
    accs3 = reshape(accs3,3,[]);
    accs5 = reshape(accs5,3,[]);
    mean(accs2,1)
    std(accs2,1)
    mean(accs3,1)
    std(accs3,1)
    mean(accs5,1)
    std(accs5,1)
end