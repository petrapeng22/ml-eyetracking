function plotResult()
    ini0File = fopen('..\exp-results\result_ini0.txt','r');
    ini1File = fopen('..\exp-results\result_ini1.txt','r');
    st0File = fopen('..\exp-results\result_st0.txt','r');
    st1File = fopen('..\exp-results\result_st1.txt','r');
    %cnnFile = fopen('result-edges.txt','r');
    formatSpec = '%f';
    ini0Result = fscanf(ini0File,formatSpec);
    ini1Result = fscanf(ini1File,formatSpec);
    st0Result = fscanf(st0File,formatSpec);
    st1Result = fscanf(st1File,formatSpec);
    %cnnResult = fscanf(cnnFile,formatSpec);
    %mean(cnnResult)
    %std(cnnResult)
    X = [ini0Result;ini1Result;st0Result; st1Result];
    %X = svmResult;
    size(X)
    G = [repmat({'ORI-BD'},600,1); repmat({'DA-BD'},600,1);repmat({'ORI-MD'},600,1);repmat({'DA-MD'},600,1)];
    %G = repmat({'Image'},500,1);
    max(ini0Result)
    size(X)
    size(G)
    boxplot(X, G);
    set('fontsize',24);
end
