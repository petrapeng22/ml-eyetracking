function getEdge()
    featureFile = matfile("../data/allFeatures_2.mat");
    allFeatures = featureFile.allFeatures;
    allEdges = [];
    for i = 1:2000
        img(:,:) = allFeatures(i,:,:);
        
        %allEdges(i,:,:) = double(edge(img,'Canny'));
        [Gmag,Gdir] = imgradient(img,'sobel');
        allEdges(i,:,:) = Gmag(:,:);
    end
    save('../data/allEdges.mat','allEdges');
end