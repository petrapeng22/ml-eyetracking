function getDataset()
    video = VideoReader('camvideo21.avi');
    fi=fopen('Annotation21.txt');
    %labelFileID = fopen('Labels11.txt','w');
    %featureFileID1 = fopen('Features11.txt','w');
    
    Features=[];
    Labels=[];
    idx=0;
    while ~feof(fi)
        line = fgetl(fi);
        img = readFrame(video);
        arr=strsplit(line,';');
        akx=str2double(arr{1,2});
        aky=str2double(arr{1,3});

        img = rgb2gray(img);
        [iy ix]=size(img);
        img=imresize(img,[iy/20 ix/20]);

        img=(double(img)/256.0);

        idx=idx+1;
        % store the whole image (every pixel) as feature with the corresponding id
        Features(idx, :, :) = img(:,:);
        % store the x-, y-coridinate as the labels of an image
        Labels(idx, 1) = akx/20; % x-coordinate of the annotated pupil center
        Labels(idx, 2) = aky/20; % y-coordinate of the annotated pupil center
    end
    
    %Labels(52,:) = Labels(101,:);
    %Features(52,:,:) = Features(101,:,:);
    
  
    save('Labels21.mat','Labels');
    save('Features21.mat','Features');
end